
from django.urls import path, re_path

from . import views

urlpatterns = [
    path('', views.market_home),
    path('<test>', views.market_home),

    re_path(r'^details/(?P<app_id>.*)/(?P<version>.*)/$', views.app_detail),
    re_path(r'^details/(?P<safe_name>.*)/$', views.app_details),

    path('apps/', views.list_apps),
    path('json/<json_pretty>', views.return_json),

    re_path(r'^apps/search/$', views.app_search),
    re_path(r'^apps/category/$', views.app_category),
    re_path(r'^apps/manage_app/$', views.manage_app),
    re_path(r'^apps/related_apps/$', views.related_apps),
    re_path(r'^apps/create/$', views.create_app),
    re_path(r'^apps/(?P<app_id>.*)/(?P<version>.*)/publish/$', views.publish_app),
    re_path(r'^apps/edit/(?P<app_id>.*)/(?P<version>.*)/$', views.update_app),
    re_path(r'^apps/(?P<app_id>.*)/(?P<version>.*)/delete/$', views.delete_app),
    re_path(r'^apps/(?P<app_id>.*)/delete/$', views.delete_app),
    re_path(r'^apps/upload/$', views.upload_file),
    re_path(r'^apps/(?P<app_id>.*)/(?P<status>.*)/suspend/$', views.suspend_app),

    path('buy/', views.buy),

]

