from django.shortcuts import render, redirect
from .config import credentials
from functools import wraps

def validate_market_place(view_func):
    def _decorator(request, *args, **kwargs):
        # maybe do something before the view_func call
        if credentials.DEVELOPER_ID is '' or credentials.SECRET is '':
            return render(request, 'api_key_error.html',{})
        response = view_func(request, *args, **kwargs)
        # maybe do something after the view_func call
        return response
    return wraps(view_func)(_decorator)
