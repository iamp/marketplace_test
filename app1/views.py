
# code taken from https://support.openchannel.io/guides/pythondjango-code-sample/

from django.shortcuts import render, redirect
from .config import requestbuilder, credentials
from .decorators import validate_market_place
import requests
import urllib
import json
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
import base64
import querystring



auth = (credentials.MARKETPLACE_ID, credentials.SECRET)

def buy(request):

    api_root = credentials.api_root

    b64_auth_str = credentials.b64_auth_str


    url = "softwaremodules/"

    # this works:
    data = '[{"vendor" : "PxC",\n  "name" : "ProductionPal3",\n  "description" : "Make your Production better",\n  "type" : "application",\n  "version" : "v0.0.1"\n}]'

    headers = {
        'Content-Type': 'application/json;charset=UTF-8',
        "Authorization": "Basic %s" % b64_auth_str
    }

    r = requests.post(api_root+url, headers=headers, data=data)  #, verify=False   params=payload

    response = r.json()
    print(r.status_code, r.url) 
    print(json.dumps(response, indent=4))

    return HttpResponse("App transferred to your hawkBit backend. " + str(r.status_code))






def total_view_count(stats):
    ''' function for counting the total views'''
    return int(sum(data[1] for data in stats))

def get_app_stats(appId=None):
    ''' This function returns the stats data and total view count'''
    if appId:
        stats_response = requests.get(requestbuilder.create_request('/stats/series/month/views?query='+urllib.parse.quote("{appId: '"+appId+"'}", safe='~()*!.\'')), auth=auth, headers=requestbuilder.REQUEST_HEADERS)
    else:
        stats_response = requests.get(requestbuilder.create_request('/stats/series/month/views?query='+urllib.parse.quote("{developerId: '"+credentials.DEVELOPER_ID+"'}", safe='~()*!.\'')), auth=auth, headers=requestbuilder.REQUEST_HEADERS)
    stats = stats_response.json()
    data = {
        'stats_data':stats,
        'views':total_view_count(stats)
    }
    return data

def app_category(request):
    search_data = urllib.parse.quote(request.GET['category'], safe='')
    string = ''
    # listed all the collections in if part
    if (search_data == 'all') or (search_data == 'myapps') or (search_data == 'popular') or (search_data == 'featured') or (search_data == 'hfeatured'):
        if search_data == 'all':
            response = requests.get(requestbuilder.create_request('/apps/?developerId='+credentials.DEVELOPER_ID+'&query={"status.value": "approved"}&sort={"randomize":1}&userId='+credentials.USER_ID), auth=auth, headers=requestbuilder.REQUEST_HEADERS)
            apps = response.json().get('list')
        if search_data == 'myapps':
            response = requests.get(requestbuilder.create_request('/apps/?developerId='+credentials.DEVELOPER_ID+'&query={"status.value": "approved"}&sort={"randomize":1}&isOwner=true&userId='+credentials.USER_ID), auth=auth, headers=requestbuilder.REQUEST_HEADERS)
            apps = response.json().get('list')
        if search_data == 'popular':
            response = requests.get(requestbuilder.create_request('/apps/?developerId='+credentials.DEVELOPER_ID+'&query={"status.value": "approved"}&sort={"statistics.views.30day": -1}'), auth=auth, headers=requestbuilder.REQUEST_HEADERS)
            apps = response.json().get('list')
        if search_data == 'featured':
            response = requests.get(requestbuilder.create_request('/apps/?developerId='+credentials.DEVELOPER_ID+'&query={"status.value": "approved", "attributes.featured":"yes"}&sort={"randomize": 1}'), auth=auth, headers=requestbuilder.REQUEST_HEADERS)
            apps = response.json().get('list')
        if search_data == 'hfeatured':
            response = requests.get(requestbuilder.create_request('/apps/?developerId='+credentials.DEVELOPER_ID+'&query={"status.value": "approved", "attributes.featured":"yes"}&sort={"randomize": 1}&limit=4'), auth=auth, headers=requestbuilder.REQUEST_HEADERS)
            apps = response.json().get('list')

            for app in apps:
                # truncated_summary = (app['customData']['summary'][:40] + '...') if len(app['customData']['summary']) > 40 else app['customData']['summary']
                string += '''
                            <div class="col-sm-6 col-md-3 MarketplaceFeature">
                                <a href="/details/%s">
                                    <div class="MarketplaceFeature-icon">
                                        <img src="%s" alt="">
                                    </div>
                                    <h3>%s</h3>
                                    <p class="MarketplaceFeature-text">%s</p>
                                </a>
                            </div> '''%(app['safeName'][0],app['customData']['icon'],app['name'],app['customData']['summary'])
    else:
        # fetching apps for categories
        search_response = requests.get(requestbuilder.create_request('/apps/?developerId='+credentials.DEVELOPER_ID+'&query={"status.value": "approved", "customData.category" :"'+search_data+'"}&fields=["name","customData.summary","customData.description"]&userId='+credentials.USER_ID), auth=auth, headers=requestbuilder.REQUEST_HEADERS)
        apps = search_response.json().get('list')
    if not string:
        for app in apps:
            # truncated_summary = (app['customData']['summary'][:40] + '...') if len(app['customData']['summary']) > 40 else app['customData']['summary']
            string += '''
                        <a class="Integration-box" href="/details/%s" >
                            <div class="row">
                                <img class="Integration-Icon" src="%s">
                                
                                <div class="Integration-Detail">
                                    <h3>%s</h3>
                                    <p class="IntegrationTagline">%s</p>
                                </div>
                            </div>
                        </a> '''%(app['safeName'][0],app['customData']['icon'],app['name'],app['customData']['summary'])

    if not string:
        string = '<p style="text-align:center;">There are no apps available</p>'
    return HttpResponse(string)

# function for searching the apps while user searched in search bar
@validate_market_place
def app_search(request):
    search_data = urllib.parse.quote(request.GET['query'], safe='')
    search_cat = urllib.parse.quote(request.GET['category'], safe='')
    if not search_cat:
        response = requests.get(requestbuilder.create_request('/apps/textSearch?developerId='+credentials.DEVELOPER_ID+"&text="+search_data+'&query={"status.value": "approved"}&fields=["name","customData.summary","customData.description"]&sort={"randomize": 1}&userId='+credentials.USER_ID), auth=auth, headers=requestbuilder.REQUEST_HEADERS)
    else:
        if (search_cat != 'myapps') or (search_cat != 'popular') or (search_cat != 'featured'):
            response = requests.get(requestbuilder.create_request('/apps/textSearch?developerId='+credentials.DEVELOPER_ID+"&text="+search_data+'&query={"status.value": "approved", "customData.category" :"'+search_cat+'"}&fields=["name","customData.summary","customData.description"]&sort={"randomize": 1}&userId='+credentials.USER_ID), auth=auth, headers=requestbuilder.REQUEST_HEADERS)
        if search_cat == 'myapps':
            response = requests.get(requestbuilder.create_request('/apps/textSearch?developerId='+credentials.DEVELOPER_ID+"&text="+search_data+'&query={"status.value": "approved"}&isOwner=true&fields=["name","customData.summary","customData.description"]&sort={"randomize": 1}&userId='+credentials.USER_ID), auth=auth, headers=requestbuilder.REQUEST_HEADERS)
        if search_cat == 'popular':
            response = requests.get(requestbuilder.create_request('/apps/textSearch/?developerId='+credentials.DEVELOPER_ID+"&text="+search_data+'&query={"status.value": "approved"}&fields=["name","customData.summary","customData.description"]&sort={"stats.views.30day": -1}'), auth=auth, headers=requestbuilder.REQUEST_HEADERS)
        if search_cat == 'featured':
            response = requests.get(requestbuilder.create_request('/apps/textSearch/?developerId='+credentials.DEVELOPER_ID+"&text="+search_data+'&query={"status.value": "approved", "attributes.featured":"yes"}&fields=["name","customData.summary","customData.description"]&sort={"randomize": 1}'), auth=auth, headers=requestbuilder.REQUEST_HEADERS)

    apps = response.json().get('list')
    string = ''
    for app in apps:
        # truncated_summary = (app['customData']['summary'][:40] + '...') if len(app['customData']['summary']) > 40 else app['customData']['summary']
        string += '''
                    <a class="Integration-box" href="/details/%s">
                        <div class="row">
                            <img class="Integration-Icon" src="%s">
                            
                            <div class="Integration-Detail">
                                <h3>%s</h3>
                                <p class="IntegrationTagline">%s</p>
                            </div>
                        </div>
                    </a> '''%(app['safeName'][0],app['customData']['icon'],app['name'],app['customData']['summary'])
    if not string:
        string = '<p style="text-align:center;">There are no apps available</p>'
    return HttpResponse(string)

def related_apps(request):
    apps = []
    search_cat = request.GET['category'].encode('utf-8').split(',')
    all_category = ''
    for cat in search_cat:
        all_category += '"'+cat.strip().replace('amp;','')+'"'+','
    search_cat = urllib.parse.quote_plus(all_category[:-1], safe='')
    search_response = requests.get(requestbuilder.create_request('/apps/?developerId='+credentials.DEVELOPER_ID+'&query={"status.value": "approved", "customData.category" :{"$in":['+search_cat+']}, "appId":{"$ne":"'+request.GET['appId'] +'" } }&limit=3&sort={"randomize":1}'), auth=auth, headers=requestbuilder.REQUEST_HEADERS)
    apps = search_response.json().get('list')
    string = ''
    for app in apps:
        if '<h4>Other Related Apps</h4>' not in string:
            string = '<h4>Other Related Apps</h4>'
        # truncated_summary = (app['customData']['summary'][:40] + '...') if len(app['customData']['summary']) > 40 else app['customData']['summary']
        string += '''
                    <div class="col-sm-6 col-md-4 MarketplaceFeature">
                        <a href="/details/%s">
                        <div class="MarketplaceFeature-icon">
                            <img src="%s" alt="">
                        </div>
                            <h3>%s</h3>
                            <p class="MarketplaceFeature-text">%s</p>
                        </a>
                    </div> '''%(app['safeName'][0],app['customData']['icon'],app['name'],app['customData']['summary'])

    return HttpResponse(string)
@validate_market_place
def market_home(request, test=None):
    version_response = requests.get(requestbuilder.create_request('/apps/?developerId='+credentials.DEVELOPER_ID+'&query={"status.value": "approved"}&sort={"randomize":1}&userId='+credentials.USER_ID), auth=auth, headers=requestbuilder.REQUEST_HEADERS)
    apps_list = version_response.json().get('list')

    json_pretty = json.dumps(version_response.json(), indent=4)

    context = { 
                'apps':apps_list,
                'json_pretty':json_pretty
            }

    if test == None:
        return render(request, 'market/index.html',context)
    else:
        return HttpResponse(json_pretty,content_type="application/json")


def return_json(request, json_pretty):
    return HttpResponse(json_pretty, content_type="application/json")


@validate_market_place
def app_details(request, safe_name):
    ''' getting the app deatils'''
    response = requests.get(requestbuilder.create_request('/apps/bySafeName/'+safe_name+'?trackViews=true&developerId='+credentials.DEVELOPER_ID+"&userId="+credentials.USER_ID), auth=auth, headers=requestbuilder.REQUEST_HEADERS)
    app_obj = response.json()

    json_pretty = json.dumps(app_obj, indent=4)


    # formating category data
    if isinstance(app_obj['customData']['categories'], list):
        app_obj['customData']['category_list'] = ", ".join(app_obj['customData']['categories'] )
        # app_obj['customData']['category_list'] = app_obj['customData']['category']

    else:
        app_obj['customData']['category_list'] = app_obj['customData']['categories'].split(',')
        app_obj['customData']['category_list'] = ", ".join(app_obj['customData']['category_list'] )
    
    #if app_obj['customData'].has_key('video'):
    #    app_obj['customData']['video'] = app_obj['customData']['video'].replace('watch?v=','embed/')
    context = { 
        'app':app_obj ,
        'disable_ownership': False,
        'json_pretty':json_pretty
    }

    return render(request, 'market/detail_page.html',context)

@validate_market_place
def app_detail(request, app_id, version):
    ''' getting the app deatils'''
    response = requests.get(requestbuilder.create_request('/apps/'+app_id+'/versions/'+version+'?developerId='+credentials.DEVELOPER_ID), auth=auth, headers=requestbuilder.REQUEST_HEADERS)

    app_obj = response.json()
    json_pretty = json.dumps(app_obj, indent=4)
    print(json_pretty)


    if isinstance(app_obj['customData']['categories'], list):
        app_obj['customData']['category_list'] = ", ".join(app_obj['customData']['categories'] )
        # app_obj['customData']['category_list'] = app_obj['customData']['category']

    else:
        app_obj['customData']['category_list'] = app_obj['customData']['categories'].split(',')
        app_obj['customData']['category_list'] = ", ".join(app_obj['customData']['category_list'] )
    #if app_obj['customData'].has_key('video'):
    #    app_obj['customData']['video'] = app_obj['customData']['video'].replace('watch?v=','embed/')
    context = { 
        'app':app_obj,
        'disable_ownership' : True,
        'json_pretty': json_pretty
    }

    return render(request, 'market/detail_page1.html',context)

@validate_market_place
def manage_app(request):
    ''' getting the app deatils'''
    toast = ''
    if request.GET['type'] == 'install':
        body = "{developerId: '%s', userId:%s, modelId:'%s'}"%(credentials.DEVELOPER_ID,credentials.USER_ID, request.GET.get('model_id', None))
        response = requests.post(requestbuilder.create_request('/ownership/apps/'+request.GET['app_id']), auth=auth, headers=requestbuilder.REQUEST_HEADERS, data=body)
        toast = "App installed successfully"
    else:
        response = requests.delete(requestbuilder.create_request('/ownership/apps/'+request.GET['app_id']+"?developerId="+credentials.DEVELOPER_ID+"&userId="+credentials.USER_ID), auth=auth, headers=requestbuilder.REQUEST_HEADERS)
        toast = "App uninstalled successfully"

    return HttpResponse(toast)

@validate_market_place
def list_apps(request):
    ''' Getting the list of all apps and their stats'''
    version_response = requests.get(requestbuilder.create_request('/apps/versions?developerId='+credentials.DEVELOPER_ID+"&query="+urllib.parse.quote('{$or: [{"status.value":"rejected",isLatestVersion:true},{isLive:true},{"status.value":{$in:["inDevelopment","inReview","pending"]}}]}&sort={status:1}', safe='~()*!.\'')), auth=auth, headers=requestbuilder.REQUEST_HEADERS)
    apps_list = version_response.json().get('list')

    draft_app = False
    for app in apps_list:
        if app['status']['value'] == 'inDevelopment':
            draft_app = True
            break
    
    toast_type = request.session.pop('toast_type', None)
    toast_message = request.session.pop('toast_message',None)
    context = { 'toast_message':toast_message,
                'toast_type':toast_type,
                'apps':apps_list,
                'draft_app':draft_app
            }
    # updating the context dict with stats and view data
    context.update(get_app_stats())
    return render(request,'developer/index.html', context)

def upload_file(request): 
    upload_response = requests.post(requestbuilder.create_request('/files'), files={request.FILES['file']._name:request.FILES['file']}, auth=auth, headers=requestbuilder.MULTIPART_HEADERS)
    return HttpResponse(upload_response.json().get('fileUrl'))

@validate_market_place
def publish_app(request, app_id, version):
    ''' Publishing the draft App'''
    body = "{developerId: '%s', version:%s}"%(credentials.DEVELOPER_ID,version)
    publish_response = requests.post(requestbuilder.create_request('/apps/' + app_id + '/publish'), auth=auth, headers=requestbuilder.REQUEST_HEADERS, data=body)
    request.session['toast_type'] = 'publish'
    if publish_response.status_code != 200:
        request.session['toast_type'] = 'error'
        request.session['toast_message'] = "There was an error publishing the app. Please try again later"
    return HttpResponse(str(publish_response.json()))

@validate_market_place
def create_app(request):
    ''' This function have two main feature : when call with GET method It render the html page for creating the app and with POST method It would create the app on oppenchannel. '''
    if request.method == 'GET':
        response = {}
        ''' rendering the html page for creating a new app'''
        return render(request,'developer/create_app.html',response)
    else:
        ''' This would run on POST call and create a new app on oppenchannel.'''
        request.session['toast_type'] = 'create'
        customData = querystring.parse_qs(request.body)
        if customData['images']:
            customData['images'] = customData['images'].split(',')
        if customData['files']:
            customData['files'] = customData['files'].split(',')

        body = {'name':customData['name'],'developerId':credentials.DEVELOPER_ID,'customData':customData}
        del body['customData']['name']
        del body['customData']['publish']
        del body['customData']['csrfmiddlewaretoken']
        # body['customData']['icon'] = 'https://d3grfap2l5ikgv.cloudfront.net/59d26b12ca753d493ed45131.jpeg'
        create_response = requests.post(requestbuilder.create_request('/apps'), auth=auth, headers=requestbuilder.REQUEST_HEADERS, data=json.dumps(body))
        customData = querystring.parse_qs(request.body)
        if create_response.status_code != 200:
            toast_msg = 'error'
            toast_message = create_response.json()['errors'][0]['message']
            response = {
                'toast_message':toast_message,
                'toast_type':toast_msg
            }
            return render(request,'developer/create_app.html',response)
        else:
            ''' publishing the app'''
            if customData['publish'] == 'true':
                request.session['toast_type'] = 'publish'
                publish_app(request,create_response.json()['appId'],create_response.json()['version'])

        return redirect('/apps/')

@validate_market_place
def get_app(app_id, version):
    ''' getting the data for an app '''
    response = requests.get(requestbuilder.create_request('/apps/'+ app_id + '/versions/'+version + '?developerId='+credentials.DEVELOPER_ID), auth=auth, headers=requestbuilder.REQUEST_HEADERS)
    return response.json()

@validate_market_place
def update_app(request, app_id, version):
    ''' updating the the app'''
    if request.method == 'GET':
        app_obj = get_app(app_id,version)
        app_obj.update(get_app_stats(app_id))
        app_obj['customData']['image_list'] = []
        app_obj['customData']['file_list'] = []
        if app_obj['customData']['images'] != '':
            app_obj['customData']['image_list'] = app_obj['customData']['images']
            app_obj['customData']['images'] = ','.join(app_obj['customData']['images'])
        
        if app_obj['customData']['files'] != '':
            app_obj['customData']['file_list'] = app_obj['customData']['files']
            app_obj['customData']['files'] = ','.join(app_obj['customData']['files'])

        return render(request,'developer/update_app.html', {'app':app_obj,'app_id':app_id,'version':version})
    else:
        customData = querystring.parse_qs(request.body)
        if customData['images']:
            customData['images'] = customData['images'].split(',')
        
        if customData['files']:
            customData['files'] = customData['files'].split(',')

        body = {'name':customData['name'], 'developerId':credentials.DEVELOPER_ID, 'customData':customData}

        del body['customData']['name']
        del body['customData']['publish']
        del body['customData']['csrfmiddlewaretoken']
        
        update_response = requests.post(requestbuilder.create_request('/apps/'+app_id+'/versions/'+version), auth=auth, headers=requestbuilder.REQUEST_HEADERS, data=json.dumps(body))
        customData = querystring.parse_qs(request.body)
        if update_response.status_code != 200:
            '''Handling response if there is problem in updating the app. '''
            
            # getting the app details
            app_obj = get_app(app_id,version)
            app_obj.update(get_app_stats())
            # showing the error message
            app_obj['toast_message'] = 'There was an error publishing the app. Please try again later'
            app_obj['toast_type'] = 'erro'
            
            response = {
                'app':app_obj,
                'app_id':app_id,
                'version':version
            }
            return render(request,'update_app.html', response)
        else:
            if customData['publish'] == 'true':
                publish_app(request,update_response.json()['appId'],update_response.json()['version'])
            request.session['toast_type'] = 'update'
        return redirect('/apps/')

@validate_market_place
def delete_app(request, app_id, version=None):
    ''' This function executes when a user deletes any app'''
    delete_request = ''
    if version:
        delete_request = requestbuilder.create_request('/apps/'+app_id+'/versions/'+version+'?developerId='+credentials.DEVELOPER_ID)
    else:
        delete_request = requestbuilder.create_request('/apps/'+app_id+'?developerId='+credentials.DEVELOPER_ID)
    delete_response = requests.delete(delete_request, auth=auth, headers=requestbuilder.REQUEST_HEADERS)
    if delete_response.status_code != 200:
        request.session['toast_type'] = 'error'
        request.session['toast_message'] = 'There was an error deleting the app. Please try again later'
    else:
        request.session['toast_type'] = 'delete'
    return HttpResponse(delete_response.json())

@validate_market_place
def suspend_app(request, app_id,status):
    ''' This function executes when a user suspend/unsuspend any app'''
    body = "{developerId: '%s', status:'%s', reason:'None'}"%(credentials.DEVELOPER_ID,status)
    suspend_request = requestbuilder.create_request('/apps/'+app_id+'/status?developerId='+credentials.DEVELOPER_ID+'&status=suspend&reason=none')
    suspend_response = requests.post(suspend_request, auth=auth, headers=requestbuilder.REQUEST_HEADERS, data=body)
    if suspend_response.status_code != 200:
        request.session['toast_type'] = 'error'
        request.session['toast_message'] = 'There was an error suspending the app. Please try again later'
    else:
        request.session['toast_type'] = 'suspend'
    return HttpResponse(suspend_response.json())


'''
delete all the application
 # for app in apps_list:
    #     delete_request = ''
    #     if app['version']:
    #         delete_request = requestbuilder.create_request('/apps/'+str(app['appId'])+'/versions/'+str(app['version'])+'?developerId='+credentials.DEVELOPER_ID)
    #     else:
    #         delete_request = requestbuilder.create_request('/apps/'+app['appId']+'?developerId='+credentials.DEVELOPER_ID)
    #     delete_response = requests.delete(delete_request, auth=auth, headers=requestbuilder.REQUEST_HEADERS)
'''