
var bSubmit = false;
var bUpdate = false;

// Disable dropzone autodiscover to initialize the dropzone with custom configuration
Dropzone.autoDiscover = false;

// Initialize tinymce editor
tinymce.init(
	{ 
		selector:'#description',
		plugins: "lists link",
		menubar: false,
		toolbar: "undo redo | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link",
		setup: function (editor) {
			editor.on('mousedown', function (e) {
				if($("textarea#description").parents(".form-group").hasClass("error")){
					$("textarea#description").parents(".form-group").removeClass("error");
					$("textarea#description").parents(".form-group").find(".help-block").text("");
				}
			});
		}
	}
);

// Create or update app
function submitApp(obj, publish) {
	bSubmit=true;
	$('#publish').val(publish);
	$(obj).find('.fa-spinner').removeClass('hidden');
	$(obj).prop('disabled', true);
	$('form').submit();
}

function initialize() {
	// Set toast message to be appeared on the top center position
	toastr.options = {
		'positionClass': 'toast-top-center'
	}
	
	// Display toast message according to various cases
	if (toast_type == 'error') {
		toastr.error(toast_message);
	} else if (toast_type == 'status') {
		toastr.success(toast_message);
	} else if (toast_type == 'create') {
		toastr.success('Your draft has been saved');
	} else if (toast_type == 'update') {
		toastr.success('Your changes have been saved');
	} else if (toast_type == 'delete') {
		toastr.success('App version deleted successfully');
	} else if (toast_type == 'publish') {
		toastr.success('Your app has been submitted and is pending approval');
	}

	// Initialize dropzones
	initializeCropperDropzone("#upload-icon", {
		fileType: "icon",
		bCropper: true,
		maxFiles: 1,
		autoProcessQueue: false,
		minWidth: 100,
		minHeight: 100
	});

	initializeCropperDropzone("#upload-images", {
		fileType: "images",
		maxFiles: 10,
		autoProcessQueue: true,
	});

	initializeCropperDropzone("#upload-files", {
		fileType: "files",
		bFile: true,
		maxFiles: 10,
		autoProcessQueue: true,
	});

	// Initialize category select dropdown
	$(".categorizer").select2();

	// Set video preview function
	$(".video-url").change(function() {
		$(".video-preview iframe").remove();
		$(".video-preview").text('');
		$(".video-preview").append(getEmbedVideoCode( $(".video-url").val(), 390, 220 ));
	});

	// Display video when the url is preset
	if ($(".video-url").val()) {
		$(".video-preview").append(getEmbedVideoCode( $(".video-url").val(), 390, 220 ));
	}

	// Initialize bootstrap tooltip
	$("[data-toggle='tooltip']").tooltip();

	// Initialize form validation
	$("form").find("input,select,textarea").not("[type='submit']").jqBootstrapValidation({ 
		preventSubmit: true,
		submitSuccess: function() {
			if (app_status != '') {
				

				// If app status is pending, submit the form without publish modal dialog.
				if (app_status == 'pending') {
					bSubmit = true;
				}
			}

			// When save button is clicked
			if (bSubmit == false && finalValidation()) {
				if(bUpdate){
					var modal =
							'<div id="modal_publish" class="modal fade" role="dialog">' +
							'	<div class="modal-dialog">' + 
							'		<div class="modal-content">' + 
							'			<div class="modal-header">' +
							'				<button class="close" data-dismiss="modal"> &times; </button>' +
							'				<h4 class="modal-title"> Publish Changes </h4>' +
							'			</div>' +
							'			<div class="modal-body">' +
							'				<p> Do you also want to publish these app changes to the marketplace right now? </p>' +
							'			</div>' +
							'			<div class="modal-footer">' +
							'				<button class="btn btn-default" onclick="submitApp(this, \'false\');"> <i class="fa fa-spinner hidden"> </i> Later </button>' +
							'				<button class="btn btn-primary" onclick="submitApp(this, \'true\');"> <i class="fa fa-spinner hidden"> </i> Yes </button>' +
							'			</div>' +
							'		</div>' +
							'	</div>' +
							'</div>';
				}else{
					var modal =
							'<div id="modal_publish" class="modal fade" role="dialog">' +
							'	<div class="modal-dialog">' + 
							'		<div class="modal-content">' + 
							'			<div class="modal-header">' +
							'				<button class="close" data-dismiss="modal"> &times; </button>' +
							'				<h4 class="modal-title"> Publish App </h4>' +
							'			</div>' +
							'			<div class="modal-body">' +
							'				<p> Do you also want to submit this app to the marketplace right now? </p>' +
							'			</div>' +
							'			<div class="modal-footer">' +
							'				<button class="btn btn-default" onclick="submitApp(this, \'false\');"> <i class="fa fa-spinner hidden"> </i> Later </button>' +
							'				<button class="btn btn-primary" onclick="submitApp(this, \'true\');"> <i class="fa fa-spinner hidden"> </i> Yes </button>' +
							'			</div>' +
							'		</div>' +
							'	</div>' +
							'</div>';

				}

				$(modal).modal();
			}
		}
	});

	$("form").submit(function(e){
		return bSubmit;
	});

	// Draw statistics flot graph
	if (statistics) {
		var xaxis = [];
		var month = (new Date()).getMonth();

		for (i = 0; i < 13; i++) {
			xaxis.push([ i, monthNames[month++ % 12] ]);
		}
		flotConfig.xaxis.ticks = xaxis;
		// stats = JSON.parse(statistics);
		stats = statistics;
		for(i = 0; i < stats.length; i++) {
			stats[i][0] = i;
		}

		$("#plot").plot(
			[
				{
					data: stats
				},
			], flotConfig
		).data("plot");
	}
}

function finalValidation(){
  websiteUrl = $("#website_url").val()  
  videoUrl = $("#video_url").val() 

  var website_pattern = /^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/;
  // var video_pattern =/^(?:https?:\/\/)?(?:m\.|www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;
  if((websiteUrl != "" && !website_pattern.test(websiteUrl)) || (videoUrl != "" && !website_pattern.test(videoUrl))){  
    return false  
} 
 return true
}

function validateWebsite(){
	var pattern =  /^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/;

	if($("#website_url").val() == "")
	{
		$('#show_website').removeClass("show")
		$('#show_website').addClass("hide")
		// $("#website_url").prop('required',false);

	}else{
		if(!pattern.test($("#website_url").val())){

			// $("#website_url").prop('required',true);
			$('#show_website').addClass("show")
			// $("#website_url").val("")

		}else{
			$('#show_website').removeClass("show")
			$('#show_website').addClass("hide")
			// $("#website_url").prop('required',false);
		}
	}
}

function validateVideo(){
	// var pattern =  /^(?:https?:\/\/)?(?:m\.|www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;
	var pattern =  /^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/;
	
	if($("#video_url").val() == "")
	{
		$('#show_video').removeClass("show")
		$('#show_video').addClass("hide")
		// $("#video_url").prop('required',false);

	}else{
		if(!pattern.test($("#video_url").val())){

			// $("#video_url").prop('required',true);
			$('#show_video').addClass("show")
			// $("#video_url").val("")

		}else{
			$('#show_video').removeClass("show")
			$('#show_video').addClass("hide")
			// $("#video_url").prop('required',false);
		}
	}
}

$(document).ready(function() {
	initialize();
});